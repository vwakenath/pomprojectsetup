package com.mpass.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mpass.utils.BaseTest;

public class BusinessProfilePage extends BaseTest{

	public BusinessProfilePage(){
		PageFactory.initElements(driver, this);
	}
	 
	@FindBy(id="editBusinessProfileButton")
	 WebElement btkBusinessProfileEdit;			 
	
	@FindBy(id="businessProfileEditButtonSave")
	 WebElement btkBusinessProfileSave;	
	
	@FindBy(xpath="//input[@id='companyWebsite']")
	 WebElement businessProfileCompanyWebsite;	
	
	
	
	public void clickEditBusinessProfileBtk(){
		btkBusinessProfileEdit.click();
	}
	
	public void clickSaveBusinessProfile(){
		btkBusinessProfileSave.click();
	}
	
	public void enterCompanyWebsite(String conpanyUrl){
		businessProfileCompanyWebsite.clear();
		businessProfileCompanyWebsite.sendKeys(conpanyUrl);
	}
}
