package com.mpass.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mpass.utils.BaseTest;

public class HomePage extends BaseTest {
	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//h4[@id='secondaryNavDashboard']")
	WebElement linkDashboard;

	@FindBy(xpath = "//h4[@id='secondaryNavKeyManagement']")
	WebElement linkKeyManagment;

	@FindBy(xpath = "//h4[@id='secondaryNavOriginUrl']")
	WebElement linkOriginUrl;

	@FindBy(xpath = "//h4[@id='secondaryNavCheckoutCredentials']")
	WebElement linkCheckoutCredentials;

	@FindBy(xpath = "//h4[@id='secondaryNavShippingProfiles']")
	WebElement linkShippingProfile;

	@FindBy(xpath = "//h4[@id='secondaryNavBusinessProfile'] ")
	WebElement linkBusinessProfile;

	@FindBy(xpath = "//h4[@id='secondaryNavPermissions'] ")
	WebElement linkPermissions;

	@FindBy(xpath = "//h4[@id='secondaryNavPaymentSettings'] ")
	WebElement linkPaymentSetting;

	@FindBy(xpath = "//h4[@id='secondaryNavProductionAccess'] ")
	WebElement linkProdAccess;

	public String getDahboardLinkText() {
		return linkDashboard.getText();
	}

	public void clickDashboardLink() {
		linkDashboard.click();
	}

	public String getKeyManagmentLinkText() {
		return linkKeyManagment.getText();
	}

	public void clickKeyManagementLink() {
		linkKeyManagment.click();
	}

	public String getOriginUrlLinkText() {
		return linkOriginUrl.getText();
	}

	public void clickOriginUrlLink() {
		linkOriginUrl.click();
	}

	public String getCheckOutCredentialsLinkText() {
		return linkCheckoutCredentials.getText();
	}

	public void clickCheckOutCredentialsLink() {
		linkCheckoutCredentials.click();
	}

	public String getShippingProfileLinkText() {
		return linkShippingProfile.getText();
	}

	public void clickShippingProfileLink() {
		linkShippingProfile.click();
	}

	public String getBusinessProfileLinkText() {
		return linkBusinessProfile.getText();
	}

	public void clickBusinessProfileLink() {
		linkBusinessProfile.click();
	}

	public String getPermissionLinkText() {
		return linkPermissions.getText();
	}

	public void clickPermissionLink() {
		linkPermissions.click();
	}

	public String getPaymentSettingsLinkText() {
		return linkPaymentSetting.getText();
	}

	public void clickPaymentSettingsLink() {
		linkPaymentSetting.click();
	}

	public String getProductionAccessLinkText() {
		return linkProdAccess.getText();
	}

	public void clickProductionAccessLink() {
		linkProdAccess.click();
	}

}
