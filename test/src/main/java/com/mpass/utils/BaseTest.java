package com.mpass.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * This is the base class conatins Browser initilization Property loader logic
 * 
 * @author Vwake
 *
 */
public class BaseTest {
	static final int PAGE_LOAD_TIMEOUT = 20;

	public static Properties prop;
	public static WebDriver driver;

	public BaseTest() {
		try {
			prop = new Properties();
			FileInputStream fis = new FileInputStream("D:\\pom\\test\\config.properties");
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void browserInit() {
		String browserName = prop.getProperty("browser");
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "D:\\pom\\test\\src\\test\\resources\\binaries\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "D:\\pom\\test\\src\\test\\resources\\binaries\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			//System.setProperty("webdriver.gecko.driver", "D:\\pom\\test\\src\\test\\resources\\binaries\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		else {
			System.out.println("Error: Browser selection invalid. Options are chrome or ie");
		}
		driver.manage().window().maximize();
		driver.get(prop.getProperty("url"));
		driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();

	}

}
