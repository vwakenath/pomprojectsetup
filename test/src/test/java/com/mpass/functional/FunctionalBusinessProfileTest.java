package com.mpass.functional;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mpass.pages.BusinessProfilePage;
import com.mpass.pages.HomePage;
import com.mpass.pages.LoginPage;
import com.mpass.utils.BaseTest;

public class FunctionalBusinessProfileTest extends BaseTest{
	
	
	LoginPage loginPage;
	HomePage homePage;
	BusinessProfilePage businessProfile;
	
	
	
	@BeforeMethod
	public void setUp(){
		browserInit();
		loginPage = new LoginPage();
		businessProfile = new BusinessProfilePage();
		homePage = new HomePage();
		loginPage.LoginToMpass(prop.getProperty("functionalUser"), prop.getProperty("funPassword"));
		loginPage.clickOnProject(prop.getProperty("projectIdFunUser"));
		
	}
	
	/**
	 * save business profile with details
	 */
	@Test(description="Functional Test save business profile flow")
	public void saveBusinessProfileTest(){
		homePage.clickBusinessProfileLink();
		businessProfile.clickEditBusinessProfileBtk();
		businessProfile.enterCompanyWebsite(prop.getProperty("websiteUrl"));
		businessProfile.clickSaveBusinessProfile();
	}
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}	

}












