package com.mpass.health.check;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mpass.pages.BusinessProfilePage;
import com.mpass.pages.HomePage;
import com.mpass.pages.LoginPage;
import com.mpass.utils.BaseTest;

public class BusinessProfileTest extends BaseTest{
	LoginPage loginPage;
	HomePage homePage;
	BusinessProfilePage businessProfile;
	
	public BusinessProfileTest() {
		// TODO Auto-generated constructor stub
		super();
	}
	@BeforeMethod
	public void setUp() {
		browserInit();
		loginPage = new LoginPage();
		businessProfile = new BusinessProfilePage();
		homePage = new HomePage();
		loginPage.LoginToMpass(prop.getProperty("healthcheckemailId"), prop.getProperty("password"));
		loginPage.clickOnProject(prop.getProperty("projectIdHealthCheckUser"));
	}
	
	/**
	 * Verify save business profile
	 */
	@Test(priority=0)
	public void SaveBusinessProfile(){
		homePage.clickBusinessProfileLink();
		businessProfile.clickEditBusinessProfileBtk();
		businessProfile.clickSaveBusinessProfile();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
