package com.mpass.health.check;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mpass.pages.HomePage;
import com.mpass.pages.LoginPage;
import com.mpass.utils.BaseTest;

public class HealthCheckTest extends BaseTest {
	LoginPage loginPage;
	HomePage homePage;

	public HealthCheckTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		BaseTest.browserInit();
		loginPage = new LoginPage();
		loginPage.LoginToMpass(BaseTest.prop.getProperty("healthcheckemailId"), BaseTest.prop.getProperty("password"));
	}

	/**
	 * Login to application and verify title
	 */
	@Test()
	public void LoginTest() {
		Assert.assertEquals(loginPage.getTitle(), prop.getProperty("title"));
	}

	/**
	 * Login and Naviagte all the tabs
	 * Verify LinkText of all the tabs
	 */
	@Test(priority = 1)
	public void NavigateToAllTheTabsAndVerifyLinkTexts() {
		homePage = loginPage.clickOnProject(prop.getProperty("projectIdHealthCheckUser"));
		homePage.clickDashboardLink();
		Assert.assertEquals(homePage.getDahboardLinkText(), "Dashboard");
		homePage.clickKeyManagementLink();
		Assert.assertEquals(homePage.getKeyManagmentLinkText(), "Key Management");
		homePage.clickOriginUrlLink();
		Assert.assertEquals(homePage.getOriginUrlLinkText(), "Origin URLs");
		homePage.clickCheckOutCredentialsLink();
		Assert.assertEquals(homePage.getCheckOutCredentialsLinkText(), "Checkout Credentials");
		homePage.clickShippingProfileLink();
		Assert.assertEquals(homePage.getShippingProfileLinkText(), "Shipping Profiles");
		homePage.clickBusinessProfileLink();
		Assert.assertEquals(homePage.getBusinessProfileLinkText(), "Business Profile");
		homePage.clickPermissionLink();
		Assert.assertEquals(homePage.getPermissionLinkText(), "Permissions");
		homePage.clickPaymentSettingsLink();
		Assert.assertEquals(homePage.getPaymentSettingsLinkText(), "Payment Settings");
		homePage.clickPermissionLink();
		Assert.assertEquals(homePage.getProductionAccessLinkText(), "Production Access");
	}
	
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
