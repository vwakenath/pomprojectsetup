package com.weather.rest.api;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.*;

@SuppressWarnings("unused")
public class GetAllCitesWeatherTest {
	String baseURI = "http://restapi.demoqa.com/utilities/weather/city/";
	ResponseSpecification respSpecForGetWeather = new ResponseSpecBuilder().expectStatusCode(200).build();
	
	/*@Test
	public void GetPuneWheatherType1() {
		//baseURI = "http://restapi.demoqa.com/utilities/weather/city/pune";
		// Creating request object
		RequestSpecification httpsRequest = RestAssured.given();
		// Creating Response Object
		Response response = httpsRequest.get(baseURI);
		// Printing Response
		String responseString = response.getBody().asString();
		System.out.println(responseString);

	}*/

	//Type 2 is quite simple to understand 
	@Test
	public void GetWeatherType2() {
		//String baseURI =  "http://restapi.demoqa.com/utilities/weather/city/pune";//or
		//baseURI = "http://restapi.demoqa.com/utilities/weather/city/pune";
		Response response = given().when().get(baseURI+"p"
				+ "une").then().assertThat().spec(respSpecForGetWeather).and().extract().response();
		String str = response.path("City");
		System.out.println(str);
	}

	@Test
	public void GetHyderabad(){
		Response response = given().when().get(baseURI+"hyderabad").then().assertThat().spec(respSpecForGetWeather).and().extract().response();
		String str = response.path("City");
		System.out.println("2222222"+str);
	}
	
	@Test
	public void GetGoa(){
		Response response = given().when().get(baseURI+"goa").then().assertThat().spec(respSpecForGetWeather).and().extract().response();
		String str = response.path("City");
		System.out.println("2222222"+str);
	}
	
	
	
}
